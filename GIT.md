# Git

This is a short Git beginners guide that focuses on concepts instead of
commands.

## What is Git

Git is a distributed version control system.

A version control system allows to manage the history of your project. For
example, you can record new versions or view older versions of your project.

```
Image of the history of a project
```

In a distributed version control system the users share the entire history of
the project.

```
Show a user sharing the entire history
```

Being distributed means that you can share the entire history of your project
with anyone. This enables any number of users to contribute to one project
asynchronously.

```
Image showing the kernel project
```

## Concepts

### Repository

A repository is a history of changes of one project.

```
Image showing the a repository
```

Conceptually Git stores a sequence of project snapshots, not individual file
changes. While not obvious at first this has important implications. For
example, Git makes it obvious that a repository corresponds to a single
project, not multiple projects.

The `commit` command is used to save the state of the project, creating a new
snapshot with meta data.

```
Image showing the commit meta data
```

The use must input the commit message. Everything else is inferred. Since we're
on the subject, the commit message is actually split into title and
description. Knowing this is important because it has an impact when showing
the commits with the `log` command. For now, here's an example of a well
written commit message:

```
```

A repository may have multiple branches.

```
Image showing multiple branches
```

A branch a sequence of commits. By default the root branch is called `master`
and the other branches derive from it. Usually a branch is created to develop a
new feature.

A tag is an alias given to a commit. The most common tag use is to mark a
commit with a traditional human readable version.

```
Image of a tags in a branch
```

Like a commit, a tag also has meta data.

```
Image of tag meta data
```

### Working directory

A working directory  is a directory that contains the repository data (in the
`.git` hidden directory) and one expanded version of it. The version you are
working on is called `HEAD`.

```
Image of HEAD and working directory
```

If you don't change anything in the working directory, the files will
correspond exactly to the latest version of the active branch. If you want to
commit a new version you have to change the state of your files first.

The files in your working directory are in one of the following states.

- Untracked: the file has never been in the repository
- Unmodified: the file is the same as the last version
- Modified: the file has been modified but it will not be in the next commit
- Staged: the file has been modified and will be in the next commit

It's very important to understand file states. A file must be staged to be
included in the next commit. Otherwise the file will simply stay in the same
state it was before the commit. To stage a file you use the `add` command.

### Remote

A remote is a repository from another host.

```
Image of remotes
```

Usually most modifications are done in your local repository and that includes
creating branches, tagging or committing. This means so far all described
concepts have been about your local repository. None had any impact on remotes.

There are three important remote commands: `clone`, `fetch` and `push`.

If you want to work on a project that belongs to a remote you must clone his
repository by cloning it. That means exactly what it seems, you will copy the
entire project history to yourself. Your repository will also memorize the
location of the remote repository you cloned from, called `origin` by default.

Both you and the remote may commit new versions to each respective repository.
If you wan to see what the remote has been doing you have to synchronize it
first. That's what the `fetch` command does. By default, when you don't
explicitly specify any remote, `fetch` will synchronize from `origin`. That's
because `clone` defines `origin` has the default `fetch` remote.

Now that you have synchronized `origin` you may want to get his changes and to
do that you `merge` one of his branches with your own. By default, if you don't
specify the remote branch to merge, it will be a branch you are tracking, which
is usually a branch with the same name as yours.

If both of you made modifications on the same files you will get a merge
conflict, that you will have to resolve in order to complete the merge.

It's also possible to `pull` a branch from a remote but there's really nothing
special about it because it's just a combination of `fetch` and `merge`. It's
just a shortcut.

If you want to do the opposite, that is, send your modifications to a remote,
you `push` your branch. However that's only possible if there are no merge
conflicts. If you want to `push` you have to `pull` first.

Notice that so far a server has not been mentioned at all. That's because there
are no servers on Git, only remotes. A project may have one remote everyone
knows and cloned from and that remote may reside in a server, but as far as Git
is concerned it's just a remote like any other. While convenient, having a
common known remote is not necessary.

Because of the decentralized nature of Git there are many possible work flows.
These are the most common:

- Centralized Workflow:
- Integration Manager Workflow: 
- Dictator and Lieutenants Workflow: 

## Commands 

Git commands can be grouped in two: local and remote. Local commands enable you
to work in your repository and remote commands to interact with other
repositories.

### Local

#### How do I start a repository?

To start a repository from scratch:

```sh
$ cd <project>
$ git init
```

To clone an existing repository:

```sh
$ git clone <url>
```

#### How do I commit my changes?

First you must add your changes to the staging area:

```sh
$ git add <file1> ... <fileN>
```

To check which files are in the staging area:

```sh
$ git status
```

When you're happy just commit:

```sh
$ git commit
```

There are two useful shortcuts to add files:

```sh
# To add every modified file
$ git add -u

# To add every file, including untracked
$ git add --all
```

And some useful shortcuts to commit:

```sh
# To add all modified files and then commit
$ git commit -a

# To supply the message directly
$ git commit -m "<your-message>"

# To check your modifications when editing the message
$ git commit -v
```

#### How do I view my commit history?

To see your commit history:

```sh
$ git log
```

This will show the current branch and the complete messages. There are
many variants of the log:

```sh
# shows the last 2 entries
$ git log -p 2

# shows log since 2 weeks
git log --since=2.weeks

# shows a summary of the modified files
$ git log --stat

# shows the commits in one line each
$ git log --oneline

# shows the log in a custom format:
# "<short-hash> - <author-name>, <autor-date>: <subject>"
git log --pretty=format:"%h - %an, %ar : %s"

# shows all branches
$ git log --branches

# shows all tags
$ git log --tags

# shows all remotes
$ git log --remotes

# shows everything (branches, tags, remotes)
$ git log --all
```

It's possible to find commits by title or even source code:

```sh
$ git
```

#### How do I create a branch?

#### How do I switch to another branch?

#### How do I go back to a specific commit?

#### How do I change my history?

To change the previous commit:

```sh
$ git commit --amend
```

To change a sequence of commits:

```sh
$ git rebase -i <commit>..<commit>
```

This will open an interactive editor where you can reorder, pick, reword, edit,
drop or merge commits in many ways.

To place a branch on top of the current branch:

```sh
$ git rebase <branch>
```

This reapplies all commits on top of the current commit so the time of
modification will be changed. It's useful to have the tree cleaner.

### Remote

#### How do I add other remotes?

To add a remote:

```sh
$ git remote add <name> <url>
```

Now you can check that remote code.

#### How do I get a remote code?

Having the latest references of a remote is not automatic. You need to
synchronize:

```sh
$ git fetch <remote>
```

The default fetch remote is usually origin, so the following commands do the
same:

```sh
$ git fetch
$ git fetch origin
```

You can also synchronize with every remote:

```sh
$ git fetch --all
```

Of course if you have many remotes it will take a while.

#### How do I add remote code to my repository?



#### How do I publish my modifications?
#### How do I commit by email?
